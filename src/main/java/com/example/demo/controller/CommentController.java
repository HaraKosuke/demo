package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class CommentController {
	@Autowired
	ReportService reportService;
	@Autowired
	CommentService commentService;

	//コメント画面表示
	@GetMapping("/comment/{id}")
	public ModelAndView commentform(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// コメントする投稿を取得
		Report report = reportService.editReport(id);
		// コメントする投稿をセット
		mav.addObject("formModel", report);
		// 画面遷移先を指定
		mav.setViewName("/comment");
		return mav;
	}

	// コメント投稿処理
	@PostMapping("/addcomment/{id}")
	public ModelAndView addComment(@PathVariable Integer id, @ModelAttribute("formModel") Comment comment) {
		// 投稿をテーブルに格納
		comment.setContent_id(id);
		System.out.println(id);
		commentService.saveComment(comment);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	//削除機能
	@PostMapping("/deleteComment/{id}")
	public ModelAndView deleteComment(@PathVariable("id") Integer id) {

		commentService.deleteComment(id);//UrlParameterのidをもとに投稿を削除

		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	//編集画面遷移
		@GetMapping("/editComment/{id}")
		public ModelAndView editContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// 編集するcommentを取得
		Comment comment = commentService.editComment(id);
		// 編集する投稿をセット
		mav.addObject("formModel", comment);
		// 画面遷移先を指定
		mav.setViewName("/editCommentview");
		return mav;
		}

		// 編集機能
		@PostMapping("/updateComment/{id}")
		public ModelAndView updateContent (@PathVariable Integer id, @ModelAttribute("formModel")Comment comment) {
		// UrlParameterのidを更新するentityにセット
		comment.setId(id);
		// 編集した投稿を更新
		commentService.saveComment(comment);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
		}


}
