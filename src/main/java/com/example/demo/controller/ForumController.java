package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class ForumController {
	@Autowired
	ReportService reportService;
	@Autowired
	CommentService commentService;

	// 投稿内容表示画面
	@GetMapping
	public ModelAndView top() {
		ModelAndView mav = new ModelAndView();
		// 投稿を全件取得
		List<Report> contentData = reportService.findAllReport();
		List<Comment> commentData = commentService.findAllComment();



		// 画面遷移先を指定
		mav.setViewName("/top");
		// 投稿データオブジェクトを保管
		mav.addObject("contents", contentData);
		mav.addObject("comments",commentData);
		mav.addObject("NoComment","NoComment");
		return mav;
	}

	// 新規投稿画面
	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Report report = new Report();
		// 画面遷移先を指定
		mav.setViewName("/new");
		// 準備した空のentityを保管
		mav.addObject("formModel", report);
		return mav;
	}

	// 投稿処理
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {
		// 投稿をテーブルに格納
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}
    //削除機能
	@PostMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable("id") Integer id) {
		System.out.println("きてる");

		reportService.deleteReport(id);//UrlParameterのidをもとに投稿を削除

		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}
	//編集画面遷移
	@GetMapping("/edit/{id}")
	public ModelAndView editContent(@PathVariable Integer id) {
	ModelAndView mav = new ModelAndView();
	// 編集する投稿を取得
	Report report = reportService.editReport(id);
	// 編集する投稿をセット
	mav.addObject("formModel", report);
	// 画面遷移先を指定
	mav.setViewName("/edit");
	return mav;
	}

	// 編集機能
	@PostMapping("/update/{id}")
	public ModelAndView updateContent (@PathVariable Integer id, @ModelAttribute("formModel") Report report) {
	// UrlParameterのidを更新するentityにセット
	report.setId(id);
	// 編集した投稿を更新
	reportService.saveReport(report);
	// rootへリダイレクト
	return new ModelAndView("redirect:/");
	}




}
